# ATTENTION

This project is now deprecated in favor of the revampe autodocumenter.  

Please see the new version in a seperate project:  
https://gitlab.com/keyrus-us/public/alteryx_auto_doc_revamp  